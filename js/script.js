const compras = [{
        producto: 'Leche',
        encontrado: true,
        precio: 20,
        descuento: 5
    },
    {
        producto: 'Huevo',
        encontrado: false,
        precio: 32,
        descuento: 0
    },
    {
        producto: 'Atún',
        encontrado: true,
        precio: 11.5,
        descuento: 0
    },
    {
        producto: 'Galleta',
        encontrado: true,
        precio: 15.70,
        descuento: 10
    },
    {
        producto: 'Frijol',
        encontrado: false,
        precio: 17,
        descuento: 0
    },
    {
        producto: 'Arroz',
        encontrado: true,
        precio: 11,
        descuento: 0
    },
    {
        producto: 'Gelatina',
        encontrado: true,
        precio: 9.70,
        descuento: 0
    },
    {
        producto: 'Aceite',
        encontrado: true,
        precio: 38.10,
        descuento: 15
    }
];

let noEncontrados = compras.filter(compra => !compra.encontrado);
console.log(noEncontrados);

console.log('\n-------------------------------\n')

let encontrados = compras.filter(compra => compra.encontrado);
let total = encontrados.reduce((precioTotal, compra) => {
    let descuento = (compra.precio * compra.descuento) / 100;
    return precioTotal + (compra.precio - descuento);
}, 0);

console.log(`El total de su cuenta es de: $${total}`)